// hard constants
const DEFAULT_GRID = 16;
const DEFAULT_COLOR = "#00FFFF";

// DOM consts
const container = document.querySelector(".container");
const setGridBtn = document.querySelector("#set-grid");
const clearBtn = document.querySelector("#clear-grid");
const resetBtn = document.querySelector("#reset");
const confetti = document.querySelector("#confetti");
const colorpicker = document.querySelector("#colorpicker");
const colorValue = document.querySelector(".color-value");

// var
let userGrid = 0;
let draw = false;
let color = DEFAULT_COLOR;

// buttons' event-listeners
setGridBtn.addEventListener("click", setGrid);

clearBtn.addEventListener("click", () => {
  clearContainer();
  if (userGrid) {
    drawGrid(userGrid);
  } else drawGrid(DEFAULT_GRID);

  disableButtons();
});

resetBtn.addEventListener("click", () => {
  clearContainer();
  drawGrid(DEFAULT_GRID);

  disableButtons(true);
});

confetti.addEventListener("click", () => {
  if (confetti.checked) resetBtn.disabled = false;

  colorpicker.disabled = !colorpicker.disabled;
  colorValue.classList.toggle("disabled");
});

colorpicker.addEventListener("input", (e) => {
  color = e.target.value.toUpperCase();
  colorValue.textContent = color;
});

// functions
function disableButtons(hard = false) {
  clearBtn.disabled = true;

  // check if disabling reset button is neccesary
  if (!userGrid || hard) {
    userGrid = 0;
    resetBtn.disabled = true;
  }
  if (hard) confetti.checked = false;
}

function setGrid() {
  do {
    userGrid = prompt("How many cells per side do you want?");

    if (userGrid === null) break;

    userGrid = parseInt(userGrid);
  } while (isNaN(userGrid));

  if (userGrid < 2) {
    userGrid = DEFAULT_GRID;
  }

  if (userGrid > 100) {
    userGrid = 100;
  }

  if (resetBtn.disabled) resetBtn.disabled = false; // is not neccessary disabling clearBtn

  clearContainer();
  drawGrid(userGrid);
}

function clearContainer() {
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }
}

function randomizeColor() {
  const r = Math.floor(Math.random() * 256);
  const g = Math.floor(Math.random() * 256);
  const v = Math.floor(Math.random() * 256);

  return `rgb(${r}, ${g}, ${v})`;
}

function hex2rgb(hex) {
  const r = parseInt(hex.slice(1, 3), 16);
  const g = parseInt(hex.slice(3, 5), 16);
  const v = parseInt(hex.slice(5, 7), 16);

  return `rgb(${r}, ${g}, ${v})`;
}

function setCellColor(e) {
  // check if previous cell color is the same as the actual color
  if (e.target.style.backgroundColor !== hex2rgb(color)) {
    e.target.dataset.colorIndex = 0;
  }

  const colorIndex = ++e.target.dataset.colorIndex;

  if (confetti.checked) {
    e.target.dataset.colorIndex = 0;
    e.target.style.background = randomizeColor();
  } else {
    e.target.style.background = color;
    e.target.style.filter = `opacity(${10 * colorIndex}%)`;
  }
}

function drawGrid(grid) {
  for (let i = 1; i <= grid * grid; i++) {
    const newCell = document.createElement("div");

    newCell.style = `width: calc(100% / ${grid}); aspect-ratio: 1 / 1;`;
    newCell.dataset.colorIndex = 0;
    container.appendChild(newCell);

    newCell.addEventListener("mousedown", (e) => {
      e.preventDefault();
      draw = true;

      setCellColor(e);

      if (clearBtn.disabled) clearBtn.disabled = false;
      if (resetBtn.disabled) resetBtn.disabled = false;
    });

    newCell.addEventListener("mousemove", (e) => {
      if (draw) {
        setCellColor(e);
      }
    });

    newCell.addEventListener("mouseup", () => {
      draw = false;
    });
  }
}

// main
clearBtn.disabled = true;
resetBtn.disabled = true;
colorpicker.value = color;
colorValue.textContent = color;
confetti.checked = false;

drawGrid(DEFAULT_GRID);
